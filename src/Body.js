import React from 'react'
import './Body.css'
import Bootstrap from '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import { useState } from 'react';

function Body() {

    const[email,setemail]=useState('')
    const[username,setusername]=useState('')
    const[password,setpassword]=useState('')


    function registerUser(event){
        event.preventDefault()

        var users = JSON.parse(localStorage.getItem('user2') || "[]")
        var newuser={
            username:username,
            email:email,
            password:password
        }
        users.push(newuser)
        localStorage.setItem('user2',JSON.stringify(users))

        alert("Registration successfull !")
    }


    return (
        <div>
            <div className='row justify-content-center'>
                <div className='col-md-6'>

                <img src="https://static2.bigstockphoto.com/1/7/2/large2/271858081.jpg" alt="Facebook" /> 

                </div>

                <div className='col-md-4'>

                    <h1> Register</h1>
                    <form onSubmit={registerUser}>
                    <input type="text" placeholder="username" className="form-control"
                     value={username} onChange={(e)=>{setusername(e.target.value)}} />

                    <input type="email" placeholder="Email ID" className="form-control"
                    value={email} onChange={(e)=>{setemail(e.target.value)}} />

                    <input type="password" placeholder="password" className="form-control"
                    value={password} onChange={(e)=>{setpassword(e.target.value)}} />
                   
                    <button type="submit" className="btn btn-primary">Signup</button>
                    </form>

                </div>

            </div>

        </div>
    )
}

export default Body
